FROM ubuntu:19.04
MAINTAINER Dylan R. E. Moonfire <d.moonfire@mfgames.com>

# Ubuntu needs to have the time zone information configured.
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Add in the core libraries and packages we need from Ubuntu.
RUN apt-get update -y \
    && apt-get install -y \
        awscli \
        curl \
        git \
        graphicsmagick \
        openssh-client \
        python \
        rsync \
        build-essential \
    && rm -rf /var/lib/apt/lists/*

# We want to grab Node 12, the latest LTS. We also want the latest node-gyp
# installed because it drags the CI processes out.
RUN curl -sL https://deb.nodesource.com/setup_12.x \
    | bash - \
    && apt-get install -y \
        nodejs \
    && rm -rf /var/lib/apt/lists/*
RUN npm install --global node-gyp
