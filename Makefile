IMAGE = dmoonfire/cobblestone-js
VERSION = $(shell cat VERSION)
VERSION2 = $(shell echo $(VERSION) | cut -f 1-2 -d .)
VERSION3 = $(shell echo $(VERSION) | cut -f 1 -d .)

build:
	docker build -t $(IMAGE):latest .

force:
	docker build --no-cache -t $(IMAGE):latest .

push:
	docker tag $(IMAGE):latest $(IMAGE):$(VERSION)
	docker tag $(IMAGE):latest $(IMAGE):$(VERSION2)
	docker tag $(IMAGE):latest $(IMAGE):$(VERSION3)
	docker push $(IMAGE):latest
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):$(VERSION2)
	docker push $(IMAGE):$(VERSION3)

enter:
	docker run -it -d $(IMAGE) bin/bash
	docker exec -it $$(docker ps -lq) bin/bash || true
	docker stop $$(docker ps -lq)
